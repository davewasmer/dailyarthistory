javascript:(function() {

  function launch() {

	// don't let me follow links or interact with the page
	$('*').click(function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
	});

	// record shift key
	var shiftKey = false;
	$(document).on('keyup keydown', function(e) { shiftKey = e.shiftKey; });

	// initialize data model
	var currentProp;
	var artist = prompt('Artist Name?');
	var currentObj = {};
	var fields = [ 'image', 'title', 'medium', 'date', 'size', 'location', 'notes' ];

	// set the current field's property to the given string (or append to it with "add" mode)
	function setCurrentValue(str, mode) {
		if (!mode) mode = "replace";
		if (mode == "add") currentObj[fields[currentProp]] = (currentObj[fields[currentProp]] || "") + str;
		else currentObj[fields[currentProp]] = str;
		$('#art-history-preview').text(JSON.stringify(currentObj, null, 2));
	}

	// persist the object to the Kinvey backend
	function saveRecord() {
		var t = currentObj.title;
		$.ajax({
			url: 'http://baas.kinvey.com/appdata/kid1081/artworks',
			type: 'POST',
			data: currentObj,
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Authorization', 'Basic a2lkMTA4MTpkZGE2NzI4NWI2NjI0ZjhlOWQzM2Y4Y2M4ZDVkMDhjYg==');
			},
			success: function() {
				$('#art-history-notification').text('Saved ' + t + '!');
			}
		})
	}

	// start a new record
	function newRecord() {
		currentObj = {
			artist: artist,
			sort: Math.round(Math.random()*Math.pow(10,10)),
			date_shown: 0
		};
		currentProp = 0;
	}

	// move on to the next field
	function nextField() {
		currentProp += 1;
		if (currentProp > fields.length - 1) {
			saveRecord();
			newRecord();
		}
		$('#art-history-current').text(fields[currentProp]);
	}

	// Make sure the space bar doesn't scroll
	$(document).on('keydown', function(e) {
		if (e.keyCode == 32) e.preventDefault();
	});

	// Space bar advances field, escape reverts the current field
	$(document).on('keyup', function(e) {
		// space
		if (e.keyCode == 32){
			e.preventDefault();
			nextField();
		// escape
		} else if (e.keyCode == 27) {
			currentProp -= 1;
			currentObj[fields[currentProp]] = '';
			$('#art-history-preview').text(JSON.stringify(currentObj, null, 2));
			$('#art-history-current').text(fields[currentProp]);
		}
	});

	// Save the selection or the click target's text (or img url) to the current field
	$(document).on('mouseup', function(e) {
		var value;
		if ($(e.target).is('img')) {
			value = $(e.target).attr('src');
		} else if (document.getSelection() == '') {
			value = $(e.target).text();
		} else {
			value = document.getSelection().toString();
			document.getSelection().removeAllRanges();
		}
		if (e.shiftKey) {
			setCurrentValue(value, "add");
		} else {
			setCurrentValue(value, "replace");
			nextField();
		}
	});

	// initialize the HUD
	var popup = $('body').append('<div id=\'art-history\'>');
	$("#art-history").css({ 
		position: 'fixed', 
		top: 0, 
		left: 0, 
		padding: 20,
		backgroundColor: '#efefef',
		overflow: 'scroll', 
		fontSize: 9,
		width:'100%'
	});
	$('#art-history').append('<div id=\'art-history-notification\'></div>');
	$('#art-history').append('Preview: <pre id=\'art-history-preview\'></pre>');
	$('#art-history').append('<span style=\'float:left\'>Current:</span> <div style=\'font-weight:bold\' id=\'art-history-current\'></div>');

	// start us a new record!
	newRecord();
	$('#art-history-current').text(fields[currentProp]);
  }

  // more or less stolen form jquery core and adapted by paul irish
  function getScript(url,success){
    var script=document.createElement('script');
    script.src=url;
    var head=document.getElementsByTagName('head')[0],
        done=false;
    // Attach handlers for all browsers
    script.onload=script.onreadystatechange = function(){
      if ( !done && (!this.readyState
           || this.readyState == 'loaded'
           || this.readyState == 'complete') ) {
        done=true;
        success();
        script.onload = script.onreadystatechange = null;
        head.removeChild(script);
      }
    };
    head.appendChild(script);
  }
  getScript('http://code.jquery.com/jquery-latest.min.js',launch);
})();

