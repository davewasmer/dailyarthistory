$ -> 
	# an event manager
	director = _.extend {}, Backbone.Events
	window.director = director;

	# hide the loading
	overlay = $(".overlay");

	# gives a random number between 0 and limit
	rand = (limit) ->
		Math.floor(Math.random()*limit)

	# returns the UNIX timestamp of today at midnight
	today = () ->
		d = new Date()
		d.setHours(0)
		d.setMinutes(0)
		d.setSeconds(0)
		d.setMilliseconds(0)
		Number(d)

	# shows the loading overlay and loads today's artwork
	# if there is no artwork for today, attempts to load tomorrow's
	getTodaysWork = () ->
		overlay.show();
		q = encodeURIComponent '{ "date_shown" : ' + today() + ' }'
		query
			path: "?query=#{q}&limit=1"
			type: 'GET'
			success: (data) ->
				if data.length is 0 then getNextDay()
				else director.trigger("data:loaded", data[0]);

	# finds the next artwork that hasn't been used by random sort
	# updates that record with today's date to prevent reuse
	getNextDay = () ->
		q = encodeURI '{"date_shown":"0"}'
		query
			path: "?query=#{q}&sort=sort&limit=1"
			type: "GET"
			success: (data) ->
				director.trigger("data:loaded", data[0])
				data[0].date_shown = today()
				query
					path: "/#{data[0]._id}"
					type: "PUT"
					data: JSON.stringify(data[0])
					success: (data) ->
						console.log("New picture for a new day!")

	# ajax helper to hit the Kinvey backend
	query = (options) ->
		$.ajax
			url: "http://baas.kinvey.com/appdata/kid1081/artworks#{options.path}"
			type: options.type
			data: options.data
			contentType: "application/json"
			beforeSend: (xhr) ->
				xhr.setRequestHeader 'Authorization', 'Basic a2lkMTA4MTpkZGE2NzI4NWI2NjI0ZjhlOWQzM2Y4Y2M4ZDVkMDhjYg=='
			success: (data) ->
				options.success(data)

	# render the data to the page
	render = (data) ->
		renderImage(data)
		$("[data-#{prop}]").text(data[prop]) for prop of data
		director.trigger('render:complete')

	# the image is a special case - handle the link and thumbnail urls
	renderImage = (data) ->
		if data.thumb then $("[data-image]").attr('src', data.thumb)
		else $("[data-image]").attr('src', getImgURL(data.image))
		$("[data-image-link]").attr('href', getImgURL(data.image))
		$("[data-image]").on 'load', () ->
			overlay.fadeOut()

	# given a thumbnail url, extract the full-size
	getImgURL = (str) ->
		console.log "Given img url: #{str}"
		str = str.replace("thumb/", "")
		arr = str.split('/')
		arr.pop()
		console.log "Extracted url: #{arr.join('/')}"
		arr.join('/')

	# once the page is ready, load today's work
	director.on('page:ready', getTodaysWork);

	# once today's work is loaded, render it
	director.on('data:loaded', render);

	# we are within a jQuery document.ready, so the DOM is all set
	# our script is ready, so fire page ready!
	director.trigger('page:ready')