// setup
tmp = []
trs = $(".sortable").first().find('tr')





function extract_column(col_index, name, extractor) {
	// default extractor just pulls the text from the cell
	if (!extractor) extractor = function(td){ return $(td).text() }
	// return a column extraction function that can be passed to a row loop
	return function(i,el){
		// get the cell in the column we want
		var td = $(el).find('td')[col_index-1]
		// extract the value from it
		if (!tmp[i]) tmp[i] = {};
		tmp[i][name] = extractor(td);
		console.log(tmp[i][name]);
	}
}
// requirements:
// a `tmp` array of global scope to dump into
// a `trs` jquery array of table rows

// example
//	table rows            2nd col  prop     extractor function
// trs.each(extract_column(2, "title", function(td){return td.text()}))