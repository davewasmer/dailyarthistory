(function() {

  $(function() {
    var director, getImgURL, getNextDay, getTodaysWork, overlay, query, rand, render, renderImage, today;
    director = _.extend({}, Backbone.Events);
    window.director = director;
    overlay = $(".overlay");
    rand = function(limit) {
      return Math.floor(Math.random() * limit);
    };
    today = function() {
      var d;
      d = new Date();
      d.setHours(0);
      d.setMinutes(0);
      d.setSeconds(0);
      d.setMilliseconds(0);
      return Number(d);
    };
    getTodaysWork = function() {
      var q;
      overlay.show();
      q = encodeURIComponent('{ "date_shown" : ' + today() + ' }');
      return query({
        path: "?query=" + q + "&limit=1",
        type: 'GET',
        success: function(data) {
          if (data.length === 0) {
            return getNextDay();
          } else {
            return director.trigger("data:loaded", data[0]);
          }
        }
      });
    };
    getNextDay = function() {
      var q;
      q = encodeURI('{"date_shown":"0"}');
      return query({
        path: "?query=" + q + "&sort=sort&limit=1",
        type: "GET",
        success: function(data) {
          director.trigger("data:loaded", data[0]);
          data[0].date_shown = today();
          return query({
            path: "/" + data[0]._id,
            type: "PUT",
            data: JSON.stringify(data[0]),
            success: function(data) {
              return console.log("New picture for a new day!");
            }
          });
        }
      });
    };
    query = function(options) {
      return $.ajax({
        url: "http://baas.kinvey.com/appdata/kid1081/artworks" + options.path,
        type: options.type,
        data: options.data,
        contentType: "application/json",
        beforeSend: function(xhr) {
          return xhr.setRequestHeader('Authorization', 'Basic a2lkMTA4MTpkZGE2NzI4NWI2NjI0ZjhlOWQzM2Y4Y2M4ZDVkMDhjYg==');
        },
        success: function(data) {
          return options.success(data);
        }
      });
    };
    render = function(data) {
      var prop;
      renderImage(data);
      for (prop in data) {
        $("[data-" + prop + "]").text(data[prop]);
      }
      return director.trigger('render:complete');
    };
    renderImage = function(data) {
      if (data.thumb) {
        $("[data-image]").attr('src', data.thumb);
      } else {
        $("[data-image]").attr('src', getImgURL(data.image));
      }
      $("[data-image-link]").attr('href', getImgURL(data.image));
      return $("[data-image]").on('load', function() {
        return overlay.fadeOut();
      });
    };
    getImgURL = function(str) {
      var arr;
      console.log("Given img url: " + str);
      str = str.replace("thumb/", "");
      arr = str.split('/');
      arr.pop();
      console.log("Extracted url: " + (arr.join('/')));
      return arr.join('/');
    };
    director.on('page:ready', getTodaysWork);
    director.on('data:loaded', render);
    return director.trigger('page:ready');
  });

}).call(this);
